CREATE TABLE Students(
    id INTEGER PRIMARY KEY,
    studentName CHAR(20) NOT NULL,
    studentSurname CHAR(20) NOT NULL,
    age INTEGER NOT NULL,
    studyYear INTEGER NOT NULL,   
);

INSERT INTO Students(id, studentName, studentSurname, age, studyYear) 
    VALUES ('1', 'Ilya', 'Ryabukhin', 19, 2);