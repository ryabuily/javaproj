package hello;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.HTMLDocument;

@RestController
public class CrudController {
   /* @Autowired
    JdbcTemplate jdbcTemplate;*/

    @Autowired
    StudentService studentService;
    private static Logger logger = LoggerFactory.getLogger(CrudController.class);
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public void deleteItem(@RequestParam(value = "id") long id){
        logger.error("deleting element");
        studentService.deleteStudent(id);
    }
    @RequestMapping(value = "/read", method = RequestMethod.GET)
    public List<Student> readElement(@RequestParam(value = "name" ,required = false) String name,
                                     @RequestParam(value = "id" ,required = false,defaultValue="-1") long id){
        logger.error("reading element");
        if(name!=null)
            return studentService.getAllStudentByName(name);
        else if(id != -1){
            return Collections.singletonList(studentService.findStudentByID(id));
        }
        else return new ArrayList<Student>();
    }
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public void updateElement(@RequestBody Student student){
        logger.error("updating element");
        studentService.updateStudent(student);

    }
    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public void createElement(@RequestBody Student student){
        logger.error("hit");
        studentService.addStudent(student);
    }

    @RequestMapping(value="/list", method = RequestMethod.GET)
    public List<Student> getStudentsList(){
        Iterable<Student> studs =studentService.findAllStudents();
        final ArrayList<Student> toReturn = new ArrayList<>();
        studs.forEach(toReturn::add);
        return toReturn;
    }
}
