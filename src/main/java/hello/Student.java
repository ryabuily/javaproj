package hello;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "student_Sequence")
    @SequenceGenerator(name = "student_Sequence", sequenceName = "STUDENT_SEQ")
    private Long id;

    @Column(name = "studentName")
    private String studentName;

    @Column(name = "studentSurname")
    private String studentSurname;

    @Column(name = "studyYear")
    private int studyYear;

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    @Column(name = "faculty")
    private String faculty;

    public Student(Long ID, String Name, String Surname,
                  int Year, String Faculty){
        this.id = ID;
        this.studentName = Name;
        this.studentSurname = Surname;
        this.studyYear = Year;
        this.faculty = Faculty;
    }

    public Student(){};

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentSurname() {
        return studentSurname;
    }

    public void setStudentSurname(String studentSurname) {
        this.studentSurname = studentSurname;
    }

    public int getStudyYear() {
        return studyYear;
    }

    public void setStudyYear(int studyYear) {
        this.studyYear = studyYear;
    }

    public String PrintStudent(){
        return String.format("%d, \"%s\" \"%s\" %d \"%s\"", id, studentName,
                studentSurname, studyYear,faculty);
    }
}
