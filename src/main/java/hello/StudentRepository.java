package hello;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long>{
    ArrayList<Student> findStudentsByStudentName(String studentName);
    void deleteStudentById(long id);
    Student findStudentById(long id);
    ArrayList<Student> findStudentByStudentNameAndStudyYear(String studentName, int year);
}
