package hello;

import java.util.ArrayList;

public interface StudentService{
    ArrayList<Student> getAllStudentByName(String name);
    void addStudent(Student student);
    void deleteStudent(long id);
    void updateStudent(Student student);
    Student findStudentByID(long id);
    Iterable<Student> findAllStudents();
}