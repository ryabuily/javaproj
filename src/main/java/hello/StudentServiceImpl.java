package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;


@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public ArrayList<Student> getAllStudentByName(String name){
        return studentRepository.findStudentsByStudentName(name);
    }
    @Transactional
    public void addStudent(Student student){
        studentRepository.save(student);
    }
    @Transactional
    public void deleteStudent(long id){
        studentRepository.deleteStudentById(id);
    }

    @Transactional
    public void updateStudent(Student student){
        studentRepository.save(student);
      /*  for(Student i: students){
            i.setStudentName(newName);
            studentRepository.save(i);
        }*/
    }

    @Transactional
    public Student findStudentByID(long id){
        return studentRepository.findStudentById(id);
    }

    @Transactional
    public Iterable<Student> findAllStudents(){
        return studentRepository.findAll();
    }

}
