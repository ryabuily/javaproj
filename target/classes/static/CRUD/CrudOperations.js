function CreateRequest()
{
    var Request = false;

    if (window.XMLHttpRequest)
    {
        Request = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        //Internet explorer
        try
        {
            Request = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (CatchException)
        {
            Request = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }

    if (!Request)
    {
        alert("Can't create XMLHttpRequest");
    }

    return Request;
}

function SendRequest(r_method, r_path, r_args){
    var Request = CreateRequest();

    if(!Request)
        return;

    Request.onreadystatechange =  function() {
        if (Request.readyState == 4) {
            if(Request.status == 200) {
                //r_handler(Request);
            }
            else {
                //there's an error
            }
        }
        else{
            //notify about loading
        }
    };
    if (r_args.length > 0)
        r_path += "?" + r_args;

    Request.open(r_method, r_path, true);

    if (r_method.toLowerCase() == "post")
    {

        Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
        Request.send(r_args);
    }
    else
    {
        Request.send(null);
    }
}