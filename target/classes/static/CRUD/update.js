function get(name){
    if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(window.location.search))
        return decodeURIComponent(name[1]);
}
function read(){

    var path = "tjv.database.windows.net";
    $.ajax({type:"GET", url:'/read?id='+get('id'),
            success:function (d,s,j) {
            },
            dataType:'json',
            contentType:"application/json"
        }
    ) .done(function(data) {
        $("#txt_id").val(data[0]?data[0].id:'null');
        $("#txt_NewName").val(data[0]?data[0].studentName:'null');
        $("#txt_NewSurname").val(data[0]?data[0].studentSurname:'null');
        $("#txt_NewYear").val(data[0]?data[0].studyYear:'null');
        $("#txt_NewFaculty").val(data[0]?data[0].faculty:'null');
    })
        .fail(function(e) {
            alert( "error!\n"+JSON.stringify(e) );
        })
        .always(function() {
            alert( "finished" );
        });
};

function update() {
    var path = "tjv.database.windows.net";
    var student={
        id: $("#txt_id").val(),
        studentName:$("#txt_NewName").val(),
        studentSurname:$("#txt_NewSurname").val(),
        studyYear:$("#txt_NewYear").val(),
        faculty:$("#txt_NewFaculty").val()
    };
    $.ajax({type:"POST", url:'/update',
            data:JSON.stringify(student),
            success:function (d,s,j) {

            },
            dataType:'json',
            contentType:"application/json"
        }
    ) .done(function() {

    })
        .fail(function(e) {
            alert( "error!\n"+JSON.stringify(e) );
        })
        .always(function() {

        });
    //  SendRequest("post",path, );
};
